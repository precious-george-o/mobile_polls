Sequel.migration do
  up do
    create_table(:poll_values) do
        primary_key :id
        String :value
        String :vote
        integer :poll_id
        datetime :created_on, :default=>CURRENT_TIMESTAMP
    end

    create_table(:polls) do

    end

    create_table(:poll_users) do

    end

    create_table(:users) do

    end

    create_table(:voters) do

    end
  end

  down do
     drop_table(:poll_values)
     drop_table(:polls)
     drop_table(:poll_users)
     drop_table(:users)
     drop_table(:voters)
  end

end