$:.unshift(File.expand_path('../../lib', __FILE__))

set :bind, '0.0.0.0'
set :port, 8080
require 'asset-handler'

Encoding.default_external = 'utf-8'  if defined?(::Encoding)

class Rack::Handler::WEBrick
  class << self
    alias_method :run_original, :run
  end
  def self.run(app, options={})
    options[:DoNotReverseLookup] = true
    run_original(app, options)
  end
end

class ApplicationController < Sinatra::Base

  helpers ApplicationHelpers

  app_dir = File.expand_path('../../', __FILE__)
  set :root, app_dir
  set :views, app_dir + "/views"
  set :public_folder, settings.root + '/public'


  enable :sessions, :method_override




  configure do
    set :root, File.expand_path('../../',__FILE__)
    #set :views, settings.root + '/assets'
    enable :coffeescript
    set :jsdir, 'js'
    set :css, 'css'
    set :sass, Compass.sass_engine_options
    set :scss, Compass.sass_engine_options
  end
  #register Sinatra::ConfigFile



  use AssetHandler

  not_found{ slim :not_found }
end