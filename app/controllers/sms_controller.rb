class SmsController < ApplicationController

  def get_poll(string)
    query = string.upcase
    Polls[:poll_nick => query]
  end

  def create_vote(values)
    PollValues.insert('value' => values.fetch('value'),
                      'vote' => values.fetch('vote'),
                      'poll_id' => values.fetch('poll_id'))
  end

  def get_first_string(sms_body)
    sms_body.split(" ")
  end

  get '/receive_votes' do
    f = File.new('logsms.log', 'w+')
    j_params = params
    sms_array = self.get_first_string(j_params.fetch('Body'))
    poll = self.get_poll(sms_array[0])
    poll_id = poll.nil? ? 0 : poll.id
    values = {'value' => sms_array[1].downcase, 'vote' => sms_array[2].downcase, 'poll_id' => poll_id}
    self.create_vote(values)
    f.write(values.to_json)
    f.close()
    "<Response> </Response>"
  end



end