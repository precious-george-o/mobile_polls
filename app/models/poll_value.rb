class PollValues < Sequel::Model(:poll_values)
=begin
  attr_accessor dataset

  def initialize()
    self.dataset = DB.from(:poll_values)
  end


  def insert(value, vote, poll_id)
    if(poll_id.is_a?(int))
      self.dataset.insert(:value => value, :vote => vote, :poll_id => poll_id)
    else
      return false
    end
  end

  #select values from db given an array of columns or non
  def select(column={})
    column ||= column['id'] || column['value'] || column['vote'] || column['poll_id']
    self.dataset.select(column.to_sym => column)
  end
=end
end