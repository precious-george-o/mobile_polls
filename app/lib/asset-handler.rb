class AssetHandler < Sinatra::Base


  configure do
    set :root, File.expand_path('../../',__FILE__)
    set :views, settings.root + '/assets'
    enable :coffeescript
    set :jsdir, 'js'
    set :css, 'css'
    set :sass, Compass.sass_engine_options
    set :scss, Compass.sass_engine_options
  end



  register Sinatra::AssetPack

  Compass.configuration do |c|
    c.project_path     = root
    c.sass_dir = 'assets/css'
    c.images_dir       = 'assets/images'
    c.http_generated_images_path = '/img'
    c.add_import_path  'assets/css'
  end



  assets {
    serve '/js',     from: 'assets/js'        # Default
    serve '/css',    from: 'assets/css'       # Default
    serve '/images', from: 'assets/images'    # Default
    serve '/img',    from: 'assets/img'

    # The second parameter defines where the compressed version will be served.
    # (Note: that parameter is optional, AssetPack will figure it out.)
    js :app, '/js/app.js', [
               'js/jquery-1.7.2.min.js',
               '/js/vendor/**/*.js',
               '/js/lib/**/*.js',
           # '/js/*.js'
           ]

    css :application, '/css/app.css', [
                        #'css/styles/foundation.min.css',
                        '/css/styles/**/*.css'
                    ]

    js_compression  :uglify    # :jsmin | :yui | :closure | :uglify
    css_compression :sass  # :simple | :sass | :yui | :sqwish
  }




end
