require 'rspec/core/rake_task'

RSpec::Core::RakeTask.new :specs do |task|
  task.pattern = Dir['spec/**/*_spec.rb']
end

task :default => ['specs']

APP_FILE = "app/lib/app/app.rb"
require 'sinatra/asset_snack/rake'