require 'rubygems'
require 'bundler'
Bundler.require :default, ENV['RACK_ENV'].to_sym
require 'sinatra/base'

ENV['RACK_ENV'] ||= 'development'

#config_file = 'config.yml'

app_dir = File.expand_path('../../', __FILE__)

#config_file = 'app/config.yml'

#set :env, %w{development}
DB = Sequel.connect(:adapter => 'mysql2', :user => 'presh', :password => "password", :host => "localhost" , :database => "mobile_poll")




set :root, app_dir
set :views, app_dir + "/views"
set :server, :thin
#set :public_folder, app_dir + '/public'
#set :css_dir, 'css'
#set :js_dir, 'js'
#set :images_dir, 'images'
#set :fonts_dir, 'fonts'
set :partials_dir, 'partials'


Dir.glob('./app/{models,helpers,controllers}/*.rb').each { |file| require_relative file }



#routes
map('/'){run SiteController}
map('/sms'){run SmsController}

#run application
run Sinatra::Application

