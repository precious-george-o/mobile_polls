require_relative '../../spec/spec_helper'
 describe 'Should return current db values count plus 1' do
    it 'Inserts into database' do
         expect{
             values = {:value => 'Psqaure', :vote => 'Win', :poll_id => 1}
             poll = PollValue.new(values)
             puts PollValue.dataset.insert_sql(poll)

         }.to change{PollValue.count}.by(1)
    end
  end