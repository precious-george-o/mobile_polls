#require "rubygems"
#require "sinatra"
#require "rack/test"
#require "test/unit"
#require "sequel"
ENV['RACK_ENV'] = 'test'
require 'rubygems'
require 'bundler'
Bundler.require :default, ENV['RACK_ENV'].to_sym
require 'sinatra/base'
#spec helper
DB = Sequel.connect(:adapter => 'mysql2', :user => 'presh', :password => "password", :host => "localhost" , :database => "mobile_poll")

#require_relative File.join('./app', 'app')
Dir.glob('./app/{models,helpers,controllers}/*.rb').each { |file| require  file }
puts 1
RSpec.configure do |config|
  include Rack::Test::Methods

  def app
    Sinatra::Application
  end
end